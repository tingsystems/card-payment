## Example for create charger with card, Conekta API


### API Reference
https://developers.conekta.com

### Libs:

[Conekta.js](https://developers.conekta.com/libraries/javascript) 
[Conekta.php](https://github.com/conekta/conekta-php)

### Card testing:

[For testing with cards](https://developers.conekta.com/resources/testing)


[Slide PDF ](http://bit.ly/2TtLBgy)