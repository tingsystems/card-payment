<?php
require_once("conekta-php/lib/Conekta.php");

if (!empty($_POST)) {
    $token =  $_POST["conektaTokenId"];
    \Conekta\Conekta::setApiKey("key_yzcQhBx8uk2Fraze");
    \Conekta\Conekta::setApiVersion("2.0.0");
    
    /** try {
        $customer = \Conekta\Customer::create(
          array(
            "name" => "Fulanito Pérez",
            "email" => "fulanito@conekta.com",
            "phone" => "+52181818181",
            "payment_sources" => array(
              array(
                  "type" => "card",
                  "token_id" => $token
              )
            )//payment_sources
          )//customer
        );
    } catch (\Conekta\ProccessingError $error){
        echo $error->getMesage();
    } catch (\Conekta\ParameterValidationError $error){
        echo $error->getMessage();
    } catch (\Conekta\Handler $error){
        echo $error->getMessage();
    }*/
    
    $valid_order = array(
        'line_items'=> array(
            array(
                'name'        => 'Box of Cohiba S1s',
                'description' => 'Imported From Mex.',
                'unit_price'  => 20000,
                'quantity'    => 1,
                'sku'         => 'cohb_s1',
                'category'    => 'food',
                'tags'        => array('food', 'mexican food')
                )
           ),
          'currency'    => 'mxn',
          'metadata'    => array('test' => 'extra info'),
          'charges'     => array(
              array(
                  'payment_method' => array(
                      'type' => 'default',
                   ),
                   'amount' => 20000
                )
            ),
            'currency'      => 'mxn',
            'customer_info' => array('customer_id'  =>'cus_2jeRFLAGwXPjj6wvW')
    );

    try {
        $order = \Conekta\Order::create($valid_order);
        
        echo "ID: ". $order->id;
        echo "Status: ". $order->payment_status;
        echo "$". $order->amount/100 . $order->currency;
        echo "Order";
        echo $order->line_items[0]->quantity .
            "-". $order->line_items[0]->name .
            "- $". $order->line_items[0]->unit_price/100;
        echo "Payment info";
        echo "CODE:". $order->charges[0]->payment_method->auth_code;
        echo "Card info:" .
            "- ". $order->charges[0]->payment_method->name .
            "- ". $order->charges[0]->payment_method->last4 .
            "- ". $order->charges[0]->payment_method->brand .
            "- ". $order->charges[0]->payment_method->type;

    } catch (\Conekta\ProcessingError $error){
        echo $error->getMessage();
    } catch (\Conekta\ParameterValidationError $error){
        echo $error->getMessage();
    } catch (\Conekta\Handler $error){
        echo $error->getMessage();
    }
}
?>


